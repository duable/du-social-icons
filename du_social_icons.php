<?php

/**
 * Wrapper function for du_trust_social_nav class
 */
function du_social_nav( $networks = array( 'facebook', 'twitter' ), $classes = array(), $icon=null ) {
  $social_nav = new du_social_icons( $networks, $classes, $icon );
  $social_nav->display();
}

/**
 * We will output a nav element with list items for each requested
 * (and available) social network link.
 * 
 * REQUIRES FontAwesome!
 */
class du_social_icons{ 

  public function __construct( $networks, $classes, $icon ) {

    /**
     * Set the network link variables
     */
    $this->facebook     = du::social()->facebook;
    $this->twitter      = du::social()->twitter;
    $this->instagram    = du::social()->instagram;
    $this->pinterest    = du::social()->pinterest;
    $this->youtube      = du::social()->youtube;
    $this->linkedin     = du::social()->linkedin;
    $this->google_plus  = du::social()->google_plus;
    $this->vimeo        = du::social()->vimeo;
    $this->pinterest    = du::social()->pinterest;
    $this->tumblr       = du::social()->tumblr;

    /**
     * Grab the specified networks and classes
     */
    if ( !is_array( $networks ) ) 
      $networks = explode( ",", str_replace( ' ', '', $networks ) );
    $this->networks   = $networks;

    if ( !empty( $classes ) && !is_array( $classes ) ) 
      $classes = explode( ",", str_replace( ' ', '', $classes ) );
    $this->classes    = implode( " ", $classes );

    $this->icon = $icon;
  }

  public function display() {

    /**
     * Output the container nav element with any specified classes
     */
    printf( "<nav class=\"social-icons %s\">\n<ul>\n", $this->classes );

    /**
     * For each of the networks specified, check if a link is available.
     * If link is available, output the link as a list item.
     */
    foreach ( $this->networks as $network ) {
      if ( !empty( $this->$network ) ) :
        echo "<li class=\"$network\">";
        echo '<a target="_blank" href="' . $this->$network . '"><i class="fa ' . $this->font_icons( $network ) . '"></i></a>';
        echo "</li>";
      endif;
    }

    /**
     * Close the container elements
     */
    echo "\n</ul>\n</nav>\n";

  }

  public function font_icons( $network ){
    switch ( $network ) {
      case 'google_plus' :
        $icon = 'fa-google-plus';
        break;
      
      default:
        $icon = 'fa-' . $network;
        break;
    }
    if ( !empty( $this->icon ) ) :
      switch ( $this->icon ) {
        case 'square':
          $icon .= '-square';
          break;

        default:
          break;
      }
    endif;

    return $icon;

  }
}
